package gui;

import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import calculator.Calculator;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import java.awt.Color;

public class GUI {

	private JFrame frmCalculadora;
	private Calculator calculator;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmCalculadora.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalculadora = new JFrame();
		frmCalculadora.getContentPane().setBackground(new Color(105, 105, 105));
		frmCalculadora.setTitle("Calculadora");
		frmCalculadora.setResizable(false);
		frmCalculadora.getContentPane().setFont(new Font("Consolas", Font.PLAIN, 11));
		frmCalculadora.getContentPane().setLayout(null);

		calculator = new Calculator(2);

		JLabel mainLabel = new JLabel(String.valueOf(calculator.getMainMemory()));
		mainLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		mainLabel.setFont(new Font("Consolas", Font.PLAIN, 29));
		mainLabel.setBounds(10, 22, 219, 33);
		frmCalculadora.getContentPane().add(mainLabel);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(94, 255, 94));
		panel.setBounds(10, 11, 230, 44);
		frmCalculadora.getContentPane().add(panel);

		// NUMBERS BUTTONS

		JButton zeroButton = new JButton("0");
		zeroButton.setForeground(Color.WHITE);
		zeroButton.setBackground(Color.GRAY);
		zeroButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressNumberButton("0");
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(2)));
			}
		});
		zeroButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		zeroButton.setBounds(70, 310, 50, 50);
		frmCalculadora.getContentPane().add(zeroButton);

		JButton oneButton = new JButton("1");
		oneButton.setForeground(Color.WHITE);
		oneButton.setBackground(Color.GRAY);
		oneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressNumberButton("1");
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(2)));
			}
		});
		oneButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		oneButton.setBounds(10, 249, 50, 50);
		frmCalculadora.getContentPane().add(oneButton);

		JButton twoButton = new JButton("2");
		twoButton.setForeground(Color.WHITE);
		twoButton.setBackground(Color.GRAY);
		twoButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressNumberButton("2");
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(2)));
			}
		});
		twoButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		twoButton.setBounds(70, 249, 50, 50);
		frmCalculadora.getContentPane().add(twoButton);

		JButton threeButton = new JButton("3");
		threeButton.setForeground(Color.WHITE);
		threeButton.setBackground(Color.GRAY);
		threeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressNumberButton("3");
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(2)));
			}
		});
		threeButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		threeButton.setBounds(130, 249, 50, 50);
		frmCalculadora.getContentPane().add(threeButton);

		JButton fourButton = new JButton("4");
		fourButton.setForeground(Color.WHITE);
		fourButton.setBackground(Color.GRAY);
		fourButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressNumberButton("4");
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(2)));
			}
		});
		fourButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		fourButton.setBounds(10, 188, 50, 50);
		frmCalculadora.getContentPane().add(fourButton);

		JButton fiveButton = new JButton("5");
		fiveButton.setForeground(Color.WHITE);
		fiveButton.setBackground(Color.GRAY);
		fiveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressNumberButton("5");
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(2)));
			}
		});
		fiveButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		fiveButton.setBounds(70, 188, 50, 50);
		frmCalculadora.getContentPane().add(fiveButton);

		JButton sixButton = new JButton("6");
		sixButton.setForeground(Color.WHITE);
		sixButton.setBackground(Color.GRAY);
		sixButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressNumberButton("6");
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(2)));
			}
		});
		sixButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		sixButton.setBounds(130, 188, 50, 50);
		frmCalculadora.getContentPane().add(sixButton);

		JButton sevenButton = new JButton("7");
		sevenButton.setForeground(Color.WHITE);
		sevenButton.setBackground(Color.GRAY);
		sevenButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressNumberButton("7");
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(2)));
			}
		});
		sevenButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		sevenButton.setBounds(10, 127, 50, 50);
		frmCalculadora.getContentPane().add(sevenButton);

		JButton eightButton = new JButton("8");
		eightButton.setForeground(Color.WHITE);
		eightButton.setBackground(Color.GRAY);
		eightButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressNumberButton("8");
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(2)));
			}
		});
		eightButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		eightButton.setBounds(70, 127, 50, 50);
		frmCalculadora.getContentPane().add(eightButton);

		JButton nineButton = new JButton("9");
		nineButton.setForeground(Color.WHITE);
		nineButton.setBackground(Color.GRAY);
		nineButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressNumberButton("9");
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(2)));
			}
		});
		nineButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		nineButton.setBounds(130, 127, 50, 50);
		frmCalculadora.getContentPane().add(nineButton);

		// OPERATORS NUMBERS

		JButton plusButton = new JButton("+");
		plusButton.setForeground(Color.WHITE);
		plusButton.setBackground(Color.GRAY);
		plusButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressPlusButton();
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(1)));
				calculator.checkForErrors();
			}
		});
		plusButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		plusButton.setBounds(190, 310, 50, 50);
		frmCalculadora.getContentPane().add(plusButton);

		JButton minusButton = new JButton("-");
		minusButton.setForeground(Color.WHITE);
		minusButton.setBackground(Color.GRAY);
		minusButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		minusButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressMinusButton();
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(1)));
				calculator.checkForErrors();
			}
		});
		minusButton.setBounds(190, 249, 50, 50);
		frmCalculadora.getContentPane().add(minusButton);

		JButton multiplyButton = new JButton("*");
		multiplyButton.setForeground(Color.WHITE);
		multiplyButton.setBackground(Color.GRAY);
		multiplyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressMultiplicationButton();
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(1)));
				calculator.checkForErrors();
			}
		});
		multiplyButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		multiplyButton.setBounds(190, 188, 50, 50);
		frmCalculadora.getContentPane().add(multiplyButton);

		JButton divideButton = new JButton("/");
		divideButton.setForeground(Color.WHITE);
		divideButton.setBackground(Color.GRAY);
		divideButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressDivisionButton();
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(1)));
				calculator.checkForErrors();
			}
		});
		divideButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		divideButton.setBounds(190, 127, 50, 50);
		frmCalculadora.getContentPane().add(divideButton);

		// MISC BUTTONS

		JButton equalsButton = new JButton("=");
		equalsButton.setForeground(Color.WHITE);
		equalsButton.setBackground(Color.GRAY);
		equalsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressEqualsButton();
				mainLabel.setText(String.valueOf(calculator.getMainMemory()));
				calculator.checkForErrors();
			}
		});
		equalsButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		equalsButton.setBounds(130, 310, 50, 50);
		frmCalculadora.getContentPane().add(equalsButton);

		JButton clearButton = new JButton("C");
		clearButton.setForeground(Color.WHITE);
		clearButton.setBackground(Color.GRAY);
		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressClearButton();
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(2)));
			}
		});
		clearButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		clearButton.setBounds(10, 66, 50, 50);
		frmCalculadora.getContentPane().add(clearButton);

		JButton memory1Button = new JButton("M1");
		memory1Button.setForeground(Color.WHITE);
		memory1Button.setBackground(Color.GRAY);
		memory1Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainLabel.setText(calculator.pressUserMemoryButton(1));
			}
		});
		memory1Button.setFont(new Font("Lucida Console", Font.BOLD, 12));
		memory1Button.setBounds(70, 66, 50, 50);
		frmCalculadora.getContentPane().add(memory1Button);

		JButton memory2Button = new JButton("M2");
		memory2Button.setForeground(Color.WHITE);
		memory2Button.setBackground(Color.GRAY);
		memory2Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainLabel.setText(calculator.pressUserMemoryButton(2));
			}
		});
		memory2Button.setFont(new Font("Lucida Console", Font.BOLD, 12));
		memory2Button.setBounds(130, 66, 50, 50);
		frmCalculadora.getContentPane().add(memory2Button);

		JButton commaButton = new JButton(",");
		commaButton.setForeground(Color.WHITE);
		commaButton.setBackground(Color.GRAY);
		commaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressCommaButton();
				mainLabel.setText(String.valueOf(calculator.getOperationalMemory(2)));
			}
		});
		commaButton.setFont(new Font("Lucida Console", Font.BOLD, 22));
		commaButton.setBounds(10, 310, 50, 50);
		frmCalculadora.getContentPane().add(commaButton);

		JButton historyButton = new JButton("<=");
		historyButton.setForeground(Color.WHITE);
		historyButton.setBackground(Color.GRAY);
		historyButton.setFont(new Font("Lucida Console", Font.BOLD, 12));
		historyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculator.pressHistoryButton();
				mainLabel.setText(String.valueOf(calculator.getMainMemory()));
			}
		});
		historyButton.setBounds(190, 66, 50, 50);
		frmCalculadora.getContentPane().add(historyButton);

		frmCalculadora.setBounds(100, 100, 254, 393);
		frmCalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}