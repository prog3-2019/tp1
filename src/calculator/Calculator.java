package calculator;

import java.util.Stack;

public class Calculator {

	public enum Operation {
		NONE, ADDITION, SUBSTRACTION, MULTIPLICATION, DIVISION
	}

	// INSTANCE VARIABLES
	private String[] memories;
	private String sign;
	private Operation actualOperation;
	private boolean mathError, displayError;
	private Stack<String> history;

	// CONSTRUCTOR

	public Calculator(int amountOfUserMemories) {
		memories = new String[3 + amountOfUserMemories];
		for (int i = 0; i < memories.length; i++) {
			if (i < 3) {
				memories[i] = "0";
			} else {
				memories[i] = "";
			}
		}
		sign = "";
		actualOperation = Operation.NONE;
		mathError = false;
		displayError = false;
		history = new Stack<String>();
		history.push("0");
	}

	// MAIN FUNCTIONS

	public void pressNumberButton(String number) {
		if (memories[1] == "0" && memories[2] == "0" && actualOperation == Operation.NONE) {
			clearMemories();
		}
		if (!(memories[2] == "0" && number == "0")) {
			addDigit(number);
		}
	}

	public void pressPlusButton() {
		if ((actualOperation != Operation.SUBSTRACTION && actualOperation != Operation.MULTIPLICATION
				&& actualOperation != Operation.DIVISION) || memories[2] != "0") {
			pressOperationButton(Operation.ADDITION);
		}
	}

	public void pressMinusButton() {
		if ((memories[0] == "0" && memories[2] == "0" && actualOperation != Operation.NONE)
				|| (memories[0] == "0" && memories[2] == "0" && actualOperation == Operation.NONE)
				|| (memories[1] == "0" && memories[2] == "0" && actualOperation != Operation.NONE)) {
			if (!hasSign()) {
				sign = "-";
			}
		} else {
			pressOperationButton(Operation.SUBSTRACTION);
		}
	}

	public void pressMultiplicationButton() {
		if (actualOperation != Operation.MULTIPLICATION && actualOperation != Operation.DIVISION) {
			pressOperationButton(Operation.MULTIPLICATION);
		}
	}

	public void pressDivisionButton() {
		if (actualOperation != Operation.MULTIPLICATION && actualOperation != Operation.DIVISION) {
			pressOperationButton(Operation.DIVISION);
		}
	}

	public void pressEqualsButton() {
		if (actualOperation == Operation.NONE) {
			if (memories[0] == "0") {
				memories[0] = memories[2];
			}
		} else {
			operate(Double.parseDouble(memories[1]), Double.parseDouble(memories[2]));
			updateMemories();
		}
	}

	public String pressUserMemoryButton(int userMemoryButton) {
		if (userMemoryIsEmpty(userMemoryButton)) {
			if (actualOperation == Operation.NONE) {
				saveInUserMemory(userMemoryButton, Double.parseDouble(memories[0]));
			} else {
				saveInUserMemory(userMemoryButton, Double.parseDouble(memories[1]));
			}
		} else {
			emptyUserMemory(userMemoryButton);
		}
		return showResult(userMemoryButton);
	}

	public void pressClearButton() {
		for (int i = 0; i < memories.length; i++) {
			if (i < 3) {
				memories[i] = "0";
			} else {
				memories[i] = "";
			}
		}
		history.clear();
		history.push("0");
		actualOperation = Operation.NONE;
	}

	public void checkForErrors() {
		if (mathError || displayError) {
			clearMemories();
			mathError = false;
			displayError = false;
		}
	}

	public void pressHistoryButton() {
		if (!history.empty()) {
			clearMemories();
			actualOperation = Operation.NONE;
			memories[0] = history.pop();
		}
	}

	public void pressCommaButton() {
		addDigit(".");
	}

	// AUXILIAR FUNCTIONS

	private void add(double x, double y) {
		if (!isOutOfRange(x + y)) {
			memories[1] = String.valueOf(x + y);
			memories[2] = "0";
			if (isARoundNumber(memories[1])) {
				memories[1] = roundNumber(memories[1]);
			}
			history.push(memories[1]);
		}
	}

	private void substract(double x, double y) {
		if (!isOutOfRange(x - y)) {
			memories[1] = String.valueOf(x - y);
			memories[2] = "0";
			if (isARoundNumber(memories[1])) {
				memories[1] = roundNumber(memories[1]);
			}
			history.push(memories[1]);
		}
	}

	private void multiply(double x, double y) {
		if (!isOutOfRange(x * y)) {
			memories[1] = String.valueOf(x * y);
			memories[2] = "0";
			if (isARoundNumber(memories[1])) {
				memories[1] = roundNumber(memories[1]);
			}
			history.push(memories[1]);
		}
	}

	private void divide(double x, double y) {
		if (y == 0) {
			memories[1] = "Math Error";
			mathError = true;
		} else if (!isOutOfRange(x / y)) {
			memories[1] = String.valueOf(x / y);
			if (isARoundNumber(memories[1])) {
				memories[1] = roundNumber(memories[1]);
			}
			history.push(memories[1]);
		}
		memories[2] = "0";
	}

	private void operate(double x, double y) {
		if (actualOperation != Operation.NONE) {
			if (actualOperation == Operation.ADDITION) {
				add(x, y);
			} else if (actualOperation == Operation.SUBSTRACTION) {
				substract(x, y);
			} else if (actualOperation == Operation.MULTIPLICATION) {
				multiply(x, y);
			} else {
				divide(x, y);
			}
			actualOperation = Operation.NONE;
		}
	}

	private void addDigit(String c) {
		if (memories[2] == "0") {
			if (c == ".") {
				memories[2] = "0.";
			} else {
				memories[2] = sign + c;
			}
		} else {
			String aux = memories[2];
			aux += sign + c;
			memories[2] = aux;
		}
		sign = "";
	}

	private void pressOperationButton(Operation op) {
		if (memories[1] == "0" && memories[2] == "0" && actualOperation == Operation.NONE) {
			pullMainMemoryValue();
		} else if (memories[1] == "0") {
			updateOperationalMemories();
		} else {
			operate(Double.parseDouble(memories[1]), Double.parseDouble(memories[2]));
		}
		actualOperation = op;
	}

	private boolean isOutOfRange(double value) {
		if (value > Integer.MAX_VALUE || value < Integer.MIN_VALUE) {
			memories[1] = "Display Error";
			displayError = true;
		}
		return displayError;
	}

	private void updateMemories() {
		memories[0] = memories[1];
		memories[1] = "0";
		memories[2] = "0";
	}

	private void updateOperationalMemories() {
		memories[1] = memories[2];
		memories[2] = "0";
	}

	private void pullMainMemoryValue() {
		memories[1] = memories[0];
		memories[0] = "0";
	}

	private void clearMemories() {
		memories[0] = "0";
		memories[1] = "0";
		memories[2] = "0";
	}

	private void saveInUserMemory(int userMemoryNumber, double x) {
		memories[2 + userMemoryNumber] = String.valueOf(x);
		if (isARoundNumber(memories[2 + userMemoryNumber])) {
			memories[2 + userMemoryNumber] = roundNumber(memories[2 + userMemoryNumber]);
		}
	}

	private void emptyUserMemory(int userMemoryNumber) {
		memories[2] = memories[2 + userMemoryNumber];
		memories[2 + userMemoryNumber] = "";
	}

	private String roundNumber(String number) {
		String roundNumber = "";
		for (int i = 0; i < number.length() - 2; i++) {
			roundNumber += number.charAt(i);
		}
		return roundNumber;
	}

	private String showResult(int userMemoryNumber) {
		return userMemoryIsEmpty(userMemoryNumber) ? String.valueOf(getOperationalMemory(2))
				: String.valueOf(getUserMemory(userMemoryNumber));
	}

	// SETTERS & GETTERS

	public String getMainMemory() {
		return memories[0];
	}

	public String getOperationalMemory(int operationalMemoryNumber) {
		if (operationalMemoryNumber == 1 || operationalMemoryNumber == 2) {
			return memories[operationalMemoryNumber];
		}
		throw new RuntimeException("operationalMemoryNumber must be 1 or 2");
	}

	public String getUserMemory(int userMemoryNumber) {
		return memories[2 + userMemoryNumber];
	}

	public boolean userMemoryIsEmpty(int userMemoryNumber) {
		return memories[2 + userMemoryNumber].equals("");
	}

	private boolean hasSign() {
		return memories[2].indexOf("-") != -1;
	}

	private boolean isARoundNumber(String number) {
		return number.indexOf(".") == number.length() - 2 && number.lastIndexOf("0") == number.length() - 1;
	}
}
