package tests;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import calculator.Calculator;

public class CalculatorTest {

	private Calculator calculator = new Calculator(2);

	@Test
	void pressNumberButtonTest() {
		calculator.pressNumberButton("1");
		calculator.pressMinusButton();
		calculator.pressNumberButton("2");
		calculator.pressEqualsButton();
		calculator.pressNumberButton("4");
		assertEquals("4", calculator.getOperationalMemory(2));
	}

	@Test
	void addingDigitsTest() {
		calculator.pressNumberButton("6");
		calculator.pressNumberButton("9");
		assertEquals("69", calculator.getOperationalMemory(2));
	}

	@Test
	void pressingNumbersDecimalAndOperationButtonsTest() {
		calculator.pressNumberButton("5");
		calculator.pressPlusButton();
		calculator.pressNumberButton("2");
		calculator.pressCommaButton();
		calculator.pressNumberButton("5");
		calculator.pressEqualsButton();
		assertEquals("7.5", calculator.getMainMemory());
	}

	@Test
	void pressingMinusButtonTest() {
		calculator.pressNumberButton("5");
		calculator.pressMinusButton();
		calculator.pressNumberButton("2");
		calculator.pressEqualsButton();
		assertEquals("3", calculator.getMainMemory());
	}

	@Test
	void usingNegativeNumbersTest() {
		calculator.pressMinusButton();
		calculator.pressNumberButton("5");
		calculator.pressMinusButton();
		calculator.pressNumberButton("2");
		calculator.pressEqualsButton();
		assertEquals("-7", calculator.getMainMemory());
	}

	@Test
	void operationsChainTest() {
		calculator.pressNumberButton("5");
		calculator.pressPlusButton();
		calculator.pressNumberButton("2");
		calculator.pressMultiplicationButton();
		calculator.pressNumberButton("2");
		calculator.pressEqualsButton();
		assertEquals("14", calculator.getMainMemory());
	}

	@Test
	void addACommaToAZeroTest() {
		calculator.pressCommaButton();
		calculator.pressNumberButton("5");
		calculator.pressPlusButton();
		calculator.pressNumberButton("2");
		calculator.pressEqualsButton();
		assertEquals("2.5", calculator.getMainMemory());
	}

	@Test
	void mathErrorTest() {
		calculator.pressNumberButton("5");
		calculator.pressDivisionButton();
		calculator.pressNumberButton("0");
		calculator.pressEqualsButton();
		calculator.checkForErrors();
		int suma = Integer.valueOf(calculator.getMainMemory()) + Integer.valueOf(calculator.getOperationalMemory(1))
				+ Integer.valueOf(calculator.getOperationalMemory(2));
		assertEquals(String.valueOf(suma), calculator.getMainMemory());
	}

	@Test
	void displayErrorTest() {
		calculator.pressNumberButton("9");
		calculator.pressNumberButton("9");
		calculator.pressNumberButton("9");
		calculator.pressNumberButton("9");
		calculator.pressNumberButton("9");
		calculator.pressMultiplicationButton();
		calculator.pressNumberButton("9");
		calculator.pressNumberButton("9");
		calculator.pressNumberButton("9");
		calculator.pressNumberButton("9");
		calculator.pressNumberButton("9");
		calculator.pressEqualsButton();
		calculator.checkForErrors();
		int suma = Integer.valueOf(calculator.getMainMemory()) + Integer.valueOf(calculator.getOperationalMemory(1))
				+ Integer.valueOf(calculator.getOperationalMemory(2));
		assertEquals(String.valueOf(suma), calculator.getMainMemory());
	}

	@Test
	void pressEqualsWithoutHavingDoneAnythingBeforeTest() {
		calculator.pressNumberButton("5");
		calculator.pressEqualsButton();
		assertEquals("5", calculator.getMainMemory());
	}

	@Test
	void saveInUserMemoryTest() {
		calculator.pressNumberButton("5");
		calculator.pressPlusButton();
		calculator.pressUserMemoryButton(1);
		assertEquals("5", calculator.getUserMemory(1));

	}

	@Test
	void chainedOperationsUserMemory1Test() {
		calculator.pressNumberButton("5");
		calculator.pressPlusButton();
		calculator.pressNumberButton("8");
		calculator.pressEqualsButton();
		calculator.pressUserMemoryButton(1);
		calculator.pressNumberButton("7");
		calculator.pressEqualsButton();
		assertEquals("13", calculator.getUserMemory(1));
	}

	@Test
	void emptyUserMemoryTest() {
		calculator.pressNumberButton("5");
		calculator.pressPlusButton();
		calculator.pressNumberButton("1");
		calculator.pressNumberButton("5");
		calculator.pressEqualsButton();
		calculator.pressUserMemoryButton(2);
		calculator.pressNumberButton("2");
		calculator.pressNumberButton("0");
		calculator.pressMinusButton();
		calculator.pressUserMemoryButton(2);
		assertEquals("", calculator.getUserMemory(2));
	}

	@Test
	void pressclearButtonTest() {
		calculator.pressNumberButton("8");
		calculator.pressDivisionButton();
		calculator.pressNumberButton("2");
		calculator.pressEqualsButton();
		calculator.pressClearButton();
		int suma = Integer.valueOf(calculator.getMainMemory()) + Integer.valueOf(calculator.getOperationalMemory(1))
				+ Integer.valueOf(calculator.getOperationalMemory(2));
		assertEquals(String.valueOf(suma), calculator.getMainMemory());
	}

	@Test
	void pullMainMemoryValueTest() {
		calculator.pressNumberButton("8");
		calculator.pressMultiplicationButton();
		calculator.pressNumberButton("4");
		calculator.pressEqualsButton();
		calculator.pressMinusButton();
		calculator.pressNumberButton("2");
		calculator.pressEqualsButton();
		assertEquals("30", calculator.getMainMemory());
	}

	@Test
	void pressHistoryButtonTest() {
		calculator.pressNumberButton("3");
		calculator.pressMultiplicationButton();
		calculator.pressNumberButton("4");
		calculator.pressEqualsButton();
		calculator.pressNumberButton("1");
		calculator.pressNumberButton("3");
		calculator.pressMinusButton();
		calculator.pressNumberButton("7");
		calculator.pressEqualsButton();
		calculator.pressHistoryButton();
		calculator.pressHistoryButton();
		assertEquals("12", calculator.getMainMemory());
	}

	@Test
	void hasSignTest() {
		calculator.pressMinusButton();
		calculator.pressNumberButton("13");
		calculator.pressMinusButton();
		calculator.pressMinusButton();
		calculator.pressNumberButton("2");
		assertEquals("-2", calculator.getOperationalMemory(2));
	}

	@Test
	void startANewOperationWithoutClearingManuallyTest() {
		calculator.pressNumberButton("3");
		calculator.pressMultiplicationButton();
		calculator.pressNumberButton("4");
		calculator.pressEqualsButton();
		calculator.pressNumberButton("1");
		calculator.pressNumberButton("3");
		int suma = Integer.valueOf(calculator.getMainMemory()) + Integer.valueOf(calculator.getOperationalMemory(1))
				+ Integer.valueOf(calculator.getOperationalMemory(2));
		assertEquals(String.valueOf(suma), calculator.getOperationalMemory(2));
	}

	@Test
	void getInvalidOperationalMemoryTest() {
		assertThrows(RuntimeException.class, () -> calculator.getOperationalMemory(3));
	}
}
